# Advent of Code 2019 - Day 5 - part Day 1

class Memory:
    def __init__(self, values):
        self.values = values

    def read(self, address):
        return self.values[address]

    def write(self, address, value):
        self.values[address] = value

    def contents(self):
        return self.values

class Computer:
    def __init__(self, memory):
        self.ip = 0
        self.memory = memory

    def run(self):
        running = True
        while running:
            running = self.execute(self.memory.read(self.ip))

    def execute(self, instr):
        instr = str(instr)
        opcode = int(instr[-1])
        if not len(instr) == 4:
            instr = "00000" + instr
        if opcode == 1:
            add1 = self.read_param(self.ip + 1, int(instr[-3]))
            add2 = self.read_param(self.ip + 2, int(instr[-4]))
            result_addr = self.memory.read(self.ip + 3)
            self.add(add1, add2, result_addr)
            self.ip += 4
        elif opcode == 2:
            mul1 = self.read_param(self.ip + 1, int(instr[-3]))
            mul2 = self.read_param(self.ip + 2, int(instr[-4]))
            result_addr = self.memory.read(self.ip + 3)
            self.multiply(mul1, mul2, result_addr)
            self.ip += 4
        elif opcode == 3:
            invalue = input("Input instruction: ")
            address = self.memory.read(self.ip + 1)
            self.memory.write(address, int(invalue))
            self.ip += 2
        elif opcode == 4:
            print(self.memory.read(self.memory.read(self.ip + 1)))
            self.ip += 2
        elif opcode == 99:
            #print("Done")
            return False
        else:
            #print("Error")
            return False
        return True

    def read_param(self, addr, immediate):
        if immediate:
            return self.memory.read(addr)
        else:
            return self.memory.read(self.memory.read(addr))

    def add(self, add1, add2, result_addr):
        self.memory.write(result_addr, add1 + add2)

    def multiply(self, mul1, mul2, result_addr):
        self.memory.write(result_addr, mul1 * mul2)


filename = "input"
with open(filename) as file:
    data = file.readline()
    data = data.split(",")
    data = [int(i) for i in data]
    #orig_inputs = tuple(data)
    memory = Memory(data)
    computer = Computer(memory)
    computer.run()
