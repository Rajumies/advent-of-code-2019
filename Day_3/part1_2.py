# Advent of Code 2019 - Day 3

class DoublyLinkedList:
    def __init__(self):
        self.current = None
        self.first = None
        self.next = None

    def add_node(self, pos_diff):
        newnode = Turn(pos_diff, self.current)
        if self.first is None:
            self.first = newnode
            self.current = newnode
            return

        self.current.next = newnode
        self.current = newnode

class Turn:
    def __init__(self, pos_diff, previous_turn):
        self.previous = previous_turn
        if self.previous is not None:
            self.x = self.previous.x + pos_diff[0]
            self.y = self.previous.y + pos_diff[1]
        else:
            self.x, self.y = 0, 0
        self.next = None
        # Distance to previous node for signal delay calculation
        self.delta = abs(pos_diff[0] + pos_diff[1])

def trace_wire(data):
    """
    Creates a doubly linked list from input data
    :param data: Input data split to a list
    :return: Linked list of objects representing turns in a wire
    """
    wire = DoublyLinkedList()
    dirs = "UDLR"

    wire.add_node((0, 0))
    for line in data:
        direction = line[0]
        value = int(line.strip(dirs))
        if direction == 'L' or direction == 'D':
            value = -value

        if direction == 'R' or direction == 'L':
            pos_diff = (value, 0)
        else:
            pos_diff = (0, value)
        wire.add_node(pos_diff)

    return wire



filename = "input"
with open(filename) as file:
    wire_a_data = file.readline().split(",")
    wire_b_data = file.readline().split(",")

wire_a = trace_wire(wire_a_data)
wire_b = trace_wire(wire_b_data)


distances = []
crossings = []

# Comparing wires to each other
wire_a.current = wire_a.first
while wire_a.current.next is not None:
    wire_a.current = wire_a.current.next
    wire_b.current = wire_b.first
    while wire_b.current.next is not None:
        # Previous and current node coordinate variable assignments
        wire_b.current = wire_b.current.next
        xa_prev = wire_a.current.previous.x
        ya_prev = wire_a.current.previous.y
        xa = wire_a.current.x
        ya = wire_a.current.y
        xb_prev = wire_b.current.previous.x
        yb_prev = wire_b.current.previous.y
        xb = wire_b.current.x
        yb = wire_b.current.y

        # Segments are parallel
        if (xa_prev == xa and xb_prev == xb) or (ya_prev == ya and yb_prev == yb):
            continue
        # Check if segments intersect in x-y direction
        if xa_prev > xb > xa or xa_prev < xb < xa or xb_prev > xa > xb or xb_prev < xa < xb:
            # Check if they also intersect in y-x direction
            if ya_prev > yb > ya or ya_prev < yb < ya or yb_prev > ya > yb or yb_prev < ya < yb:
                if ya_prev == ya:
                    pos = (xb, ya)
                else:
                    pos = (xa, yb)
                distances += [abs(pos[0]) + abs(pos[1])]
                crossings.append([pos, wire_a.current.previous, wire_b.current.previous])

print("\nDay 3 part 1 solution: " + str(min(distances)))

signal_delays = []
# Retracing wires back to the central port and calculating distance
for intersection in crossings:
    a_node = intersection[1]
    b_node = intersection[2]
    cross_x = intersection[0][0]
    cross_y = intersection[0][1]
    distance_a = abs(cross_x - a_node.x + cross_y - a_node.y)
    while a_node.previous is not None:
        distance_a += a_node.delta
        a_node = a_node.previous

    distance_b = abs(cross_x - b_node.x + cross_y - b_node.y)
    while b_node.previous is not None:
        distance_b += b_node.delta
        b_node = b_node.previous
    signal_delays += [distance_a + distance_b]
print("Day 3 part 2 solution: " + str(min(signal_delays)))

