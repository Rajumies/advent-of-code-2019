# Advent of Code 2019 - Day 7 - part 1 & 2

import itertools

class Memory:
    def __init__(self, values):
        self.values = values

    def read(self, address):
        return self.values[address]

    def write(self, address, value):
        self.values[address] = value

    def contents(self):
        return self.values

class Computer:
    def __init__(self, memory):
        self.ip = 0
        self.memory = memory
        self.input = []
        self.running = True
        self.output = ""
        self.halted = False

    def run(self):
        self.running = True
        while self.running:
            self.execute(self.memory.read(self.ip))

    def execute(self, instr):
        instr = str(instr)
        opcode = int(instr[-1])
        if not len(instr) == 4:
            instr = "00000" + instr
        ip_old = self.ip
        param_nums = 0
        if opcode == 1:
            add1 = self.read_param(self.ip + 1, instr[-3])
            add2 = self.read_param(self.ip + 2, instr[-4])
            result_addr = self.memory.read(self.ip + 3)
            self.add(add1, add2, result_addr)
            param_nums = 3

        elif opcode == 2:
            mul1 = self.read_param(self.ip + 1, instr[-3])
            mul2 = self.read_param(self.ip + 2, instr[-4])
            result_addr = self.memory.read(self.ip + 3)
            self.multiply(mul1, mul2, result_addr)
            param_nums = 3

        elif opcode == 3:
            invalue = self.input.pop(0)
            address = self.memory.read(self.ip + 1)
            self.memory.write(address, invalue)
            param_nums = 1

        elif opcode == 4:
            output = self.memory.read(self.memory.read(self.ip + 1))
            param_nums = 1
            self.running = False
            self.output = output


        elif opcode == 5:
            param1 = self.read_param(self.ip + 1, instr[-3])
            if param1:
                param2 = self.read_param(self.ip + 2, instr[-4])
                self.ip = param2
            param_nums = 2

        elif opcode == 6:
            param1 = self.read_param(self.ip + 1, instr[-3])
            if not param1:
                param2 = self.read_param(self.ip + 2, instr[-4])
                self.ip = param2
            param_nums = 2

        elif opcode == 7:
            param1 = self.read_param(self.ip + 1, instr[-3])
            param2 = self.read_param(self.ip + 2, instr[-4])
            result_addr = self.memory.read(self.ip + 3)
            if param1 < param2:
                self.memory.write(result_addr, 1)
            else:
                self.memory.write(result_addr, 0)
            param_nums = 3

        elif opcode == 8:
            param1 = self.read_param(self.ip + 1, instr[-3])
            param2 = self.read_param(self.ip + 2, instr[-4])
            result_addr = self.memory.read(self.ip + 3)
            if param1 == param2:
                self.memory.write(result_addr, 1)
            else:
                self.memory.write(result_addr, 0)
            param_nums = 3

        elif opcode == 99:
            self.running = False
            self.halted = True
            #self.output =""
        else:
            self.running = False
            self.halted = True
            #self.output = ""

        if self.ip == ip_old:
            self.ip += param_nums + 1

    def read_param(self, addr, immediate):
        if int(immediate):
            return self.memory.read(addr)
        else:
            return self.memory.read(self.memory.read(addr))

    def add(self, add1, add2, result_addr):
        self.memory.write(result_addr, add1 + add2)

    def multiply(self, mul1, mul2, result_addr):
        self.memory.write(result_addr, mul1 * mul2)


def part1():

    phases = list(itertools.permutations([0, 1, 2, 3, 4]))
    input_value = [0]
    best_value = 0
    memory = Memory(data.copy())
    computer = Computer(memory)
    for setting in phases:
        for phase in setting:
            input_value.insert(0, phase)
            computer.input = input_value
            computer.run()
            input_value = [computer.output]
            memory.__init__(data.copy())
            computer.__init__(memory)
        if input_value[0] > best_value:
            best_value = input_value[0]

        input_value = [0]
    print("\nDay 7 part 1 solution: " + str(best_value))

def part2():

    phases = list(itertools.permutations([5, 6, 7, 8, 9]))
    output_value = 0
    amplifiers = []
    for i in range(0, 5):
        memory = Memory(data.copy())
        amplifiers.append(Computer(memory))

    for setting in phases:
        index = 0
        input_value = 0

        for i, amplifier in enumerate(amplifiers):
            memory = Memory(data.copy())
            amplifier.__init__(memory)
            amplifier.input.append(setting[i])

        while not [x.halted for x in amplifiers] == [True, True, True, True, True]:
            amplifiers[index].input.append(input_value)
            amplifiers[index].run()
            input_value = amplifiers[index].output
            index = (index + 1) % 5

        if input_value > output_value:
            output_value = input_value
    print("\nPart 2 solution: " + str(output_value))


filename = "input"
with open(filename) as file:
    data = file.readline()
data = data.split(",")
data = [int(i) for i in data]
part1()
part2()




