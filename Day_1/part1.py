# Advent of Code 2019 - Day Day 1 - part Day 1

filename = "masses"
mass_req = 0
with open(filename) as file:
    for line in file:
        mass_req += int(int(line) / 3) - 2

print(mass_req)