# Advent of Code 2019 - Day Day 1 - part 2

# Recursive function for calculating fuel required for needed fuel etc
def fuel_calc(mass):

    fuel_req = int(mass / 3) - 2
    if fuel_req > 0:
        fuel_req += fuel_calc(fuel_req)
    # possible negative mass is 0
    else:
        fuel_req = 0
    return fuel_req

filename = "masses"
mass_req = 0
with open(filename) as file:
    for line in file:
        total_module_mass = fuel_calc(int(line))
        mass_req += total_module_mass
print(mass_req)
