# Advent of Code 2019 - Day 11 - part 1 & 2

from Day_11 import intcode
import sys

# Circular doubly linked list for robot directions
class CircularDoublyLinkedList:
    def __init__(self):
        self.current = None
        self.next = None

    # Adds a node at the end
    def add(self, direction, modifier):
        newnode = Node(direction, modifier)
        if self.current is None and self.next is None:
            self.current = newnode
            self.next = newnode
        newnode.previous = self.current
        newnode.next = self.next
        self.current.next = newnode
        self.next.previous = newnode
        self.current = newnode

    def turn_right(self):
        self.current = self.current.next
        self.next = self.current.next

    def turn_left(self):
        self.next = self.current
        self.current = self.current.previous

# Single node in the linked list, representing a direction
class Node:
    def __init__(self, direction, modifier):
        # Direction string is not actually used, it's there for readability and debugging
        self.direction = direction
        # Coordinate delta
        self.modifier = modifier
        self.next = None
        self.previous = None

class Robot:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.painted = {}
        self.direction = CircularDoublyLinkedList()
        self.direction.add("right", (1, 0))
        self.direction.add("down", (0, -1))
        self.direction.add("left", (-1, 0))
        self.direction.add("up", (0, 1))

    def turn(self, direction):
        if direction == 0:
            self.direction.turn_left()
        elif direction == 1:
            self.direction.turn_right()
        else:
            sys.exit("Error: Invalid turn code")
        self.move()

    def move(self):
        self.x += self.direction.current.modifier[0]
        self.y += self.direction.current.modifier[1]

    def paint(self, color):
        self.painted[(self.x, self.y)] = color

    def get_color(self):
        color = self.painted.get((self.x, self.y))
        if color is None:
            color = 0
        return color

filename = "input"
with open(filename) as file:
    data = file.readline()
data = data.split(",")
data = [int(i) for i in data]

### Part 1

memory = intcode.Memory(data.copy())
computer = intcode.Computer(memory)
robot = Robot()
while not computer.halted:
    current_color = robot.get_color()
    computer.input = [current_color]
    computer.run()
    if computer.halted:
        break
    robot.paint(computer.output[0])
    computer.run()
    robot.turn(computer.output[0])

print("\nDay 11 part 1 solution: " + str(len(robot.painted)))

### Part 2

memory.__init__(data.copy())
computer.__init__(memory)
robot.__init__()
robot.painted[(0, 0)] = 1

while not computer.halted:
    current_color = robot.get_color()
    computer.input = [current_color]
    computer.run()
    if computer.halted:
        break
    robot.paint(computer.output[0])
    computer.run()
    robot.turn(computer.output[0])

print("\nDay 11 part 2 solution:")
for y in range(1, -6, -1):
    row = ""
    for x in range(0, 50):
        color = robot.painted.get((x, y))
        if color is None:
            tile = " "
        elif color == 0:
            tile = " "
        else:
            tile = "#"
        row += tile
    print(row)