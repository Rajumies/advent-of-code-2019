# Advent of Code 2019 - Day 12 - part 1


class Moon:
    def __init__(self, x, y, z):
        self.pos = [x, y, z]
        self.velocity = [0, 0, 0]

def calculate_velocities(moons):
    for addressed in moons:
        for affecting in moons:
            if addressed is affecting:
                continue
            for dimension in range(0, 3):
                if addressed.pos[dimension] < affecting.pos[dimension]:
                    addressed.velocity[dimension] += 1
                elif addressed.pos[dimension] > affecting.pos[dimension]:
                    addressed.velocity[dimension] -= 1

def apply_velocities(moons):
    for moon in moons:
        for dimension in range(0,3):
            moon.pos[dimension] += moon.velocity[dimension]

def parse_input(data):
    moons = []
    for row in data:
        coords = row.split(", ")
        coords = [int(x.strip("<>xyz=\n")) for x in coords]
        moons.append(Moon(coords[0], coords[1], coords[2]))
    return moons
filename = "input"
with open(filename) as file:
    data = file.readlines()

moons = parse_input(data)

for i in range(0, 1000):
    calculate_velocities(moons)
    apply_velocities(moons)

total_energy = 0
for moon in moons:
    potential_energy = 0
    kinetic_energy = 0
    for dimension in range(0, 3):
        potential_energy += abs(moon.pos[dimension])
        kinetic_energy += abs(moon.velocity[dimension])
    total_energy += potential_energy * kinetic_energy
print("\nDay 12 part 1 solution: " + str(total_energy))


