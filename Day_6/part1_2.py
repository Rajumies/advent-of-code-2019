# Advent of Code 2019 - Day 6 - part Day 1

class Node:
    def __init__(self, name="COM"):
        self.name = name
        self.children = []
        self.parent = None
        self.depth = 0

    def add_child(self, node):
        self.children.append(node)


def depth(node, orbits):
    node.depth = orbits
    for child in node.children:
        depth(child, orbits + 1)


filename = "input"
data = []
with open(filename) as file:
    for line in file:
        line = line.rstrip()
        data.append(line.split(")"))
objects = {}
for pair in data:
    if pair[0] in objects:
        center = objects[pair[0]]
    else:
        center = Node(pair[0])
    if pair[1] in objects:
        orbiter = objects[pair[1]]
    else:
        orbiter = Node(pair[1])
    center.add_child(orbiter)
    orbiter.parent = center
    objects[pair[0]] = center
    objects[pair[1]] = orbiter

depth(objects["COM"], 0)
orbits = 0
for obj in objects:
    orbits += objects[obj].depth

print("Total orbits: " + str(orbits))

current = "YOU"
from_me = {}
transfers = 0
while not current == "COM":
    current = objects[current].parent.name
    from_me[current] = transfers
    transfers += 1

current = "SAN"
transfers = 0
while not current in from_me:
    current = objects[current].parent.name
    transfers += 1

print("Transfers to Santa: " + str(transfers - 1 + from_me[current]))
