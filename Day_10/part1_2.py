# Advent of Code 2019 - Day 10 - part 1 & 2

from math import gcd, atan, pi, isclose


class Asteroid:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.blocked_dirs = []
        self.detected = 0
        self.vaporized = False
        self.angle_from_station = 0.0

    def add_if_seen(self, asteroid):
        xdir = asteroid.x - self.x
        ydir = asteroid.y - self.y
        # Return if we're comparing to ourselves
        if xdir == 0 and ydir == 0:
            return
        xdir, ydir = self.reduce(xdir, ydir)

        for direction in self.blocked_dirs:
            if (xdir, ydir) == direction:
                return
        self.blocked_dirs.append((xdir, ydir))
        self.detected += 1

    def reduce(self, x, y):
        gcd_value = gcd(x, y)
        x = x // gcd_value
        y = y // gcd_value
        return x, y

class GiantLaser:
    def __init__(self, asteroids, x, y):
        self.asteroids = asteroids
        self.x = x
        self.y = y
        self.destroyed = 0
        self.lastdestroyed = None

    def destroy(self, goal):
        index = 0
        while not self.destroyed == goal:
            angle = self.asteroids[index].angle_from_station
            # Get all asteroids in a straight line at angle
            matches = [x for x in self.asteroids if isclose(x.angle_from_station, angle)]
            # Sort them by distance from station
            matches.sort(key=self.sort_by_distance)
            for target in matches:
                if not target.vaporized:
                    target.vaporized = True
                    self.destroyed += 1
                    self.lastdestroyed = target
                    break
            # Skip the rest
            index += len(matches)
            if index >= len(asteroids) - 1:
                index = 0

    def sort_by_distance(self, asteroid):
        return abs(self.x - asteroid.x) + abs(self.y - asteroid.y)

def test():
    filename = "testinput1"
    asteroids = read_file(filename)
    station = find_station(asteroids)
    assert (station.x, station.y) == (5, 8), "Test 1 coordinates don't match"
    assert station.detected == 33, "Test 1 amount doesn't match"

    filename = "testinput2"
    asteroids = read_file(filename)
    station = find_station(asteroids)
    assert (station.x, station.y) == (1, 2), "Test 2 coordinates don't match"
    assert station.detected == 35, "Test 2 amount doesn't match"

    filename = "testinput3"
    asteroids = read_file(filename)
    station = find_station(asteroids)
    assert (station.x, station.y) == (6, 3), "Test 3 coordinates don't match"
    assert station.detected == 41, "Test 3 amount doesn't match"

    filename = "testinput4"
    asteroids = read_file(filename)
    station = find_station(asteroids)
    assert (station.x, station.y) == (11, 13), "Test 4 coordinates don't match"
    assert station.detected == 210, "Test 4 amount doesn't match"


def read_file(filename):
    with open(filename) as file:
        data = file.readlines()
    asteroids = []
    for y, row in enumerate(data):
        for x, pos in enumerate(row):
            if pos == "#":
                asteroids.append(Asteroid(x, y))
    return asteroids

def find_station(asteroids):
    for source in asteroids:
        for target in asteroids:
            source.add_if_seen(target)
    highest = asteroids[0]
    for asteroid in asteroids:
        if asteroid.detected > highest.detected:
            highest = asteroid
    return highest

def calculate_angle(asteroid):
    dx = asteroid.x - station.x
    dy = asteroid.y - station.y

    # On the same x-axis as station
    if dy == 0:
        # Right side
        if dx > 0:
            asteroid.angle_from_station = pi / 2
        # Left side
        elif dx < 0:
            asteroid.angle_from_station = (3 * pi) / 2
        return
    # First quadrant
    if asteroid.x >= station.x and asteroid.y < station.y:
        asteroid.angle_from_station = atan(dx / abs(dy))
    # Second quadrant
    elif asteroid.x > station.x and asteroid.y > station.y:
        asteroid.angle_from_station = atan(dy / dx) + pi / 2
    # Third quadrant
    elif asteroid.x <= station.x and asteroid.y > station.y:
        asteroid.angle_from_station = atan(abs(dx) / dy) + pi
    # Fourth quadrant
    elif asteroid.x < station.x and asteroid.y < station.y:
        asteroid.angle_from_station = atan(dy / dx) + (3 * pi) / 2

def sort_by_angle(asteroid):
    return asteroid.angle_from_station

test()

### Part 1

filename = "input"
asteroids = read_file(filename)
station = find_station(asteroids)
print("\nDay 10 part 1 solution: Asteroid " + str((station.x, station.y)) + ", with " + str(station.detected) + " detected")

### Part 2

# Get asteroid field size
with open(filename) as file:
    data = file.readlines()
# -1 in x-direction to account for newline character
asteroid_belt_size = (len(data[0]) - 1, len(data))

for asteroid in asteroids:
    calculate_angle(asteroid)
asteroids.sort(key=sort_by_angle)

laser = GiantLaser(asteroids, station.x, station.y)
laser.destroy(200)
print("\nDay 10 part 2 solution: " + str(laser.lastdestroyed.x * 100 + laser.lastdestroyed.y))



