# Advent of code - Day 14 - part 1

stockpile = {}
reactions = {}
ore_required = 0

class Reaction:
    def __init__(self, produces, ingredients):
        """
        Reaction object represents a single reaction
        :param produces: Amount of stuff produced
        :param ingredients: A dictionary of ingredients {Name: amount produced}
        """
        self.produces = produces
        self.ingredients = ingredients

def calculate_required(required):
    """
    Recursive function for calculating ingredients required for a reaction
    :param required: Name of the required ingredient
    :return:
    """
    # Loop through all the required ingredients for a reaction
    for reaction in reactions[required].ingredients:
        required_amount = reactions[required].ingredients[reaction]
        if reaction == "ORE":
            # We have reached the bottom of recursion
            global ore_required
            ore_required += required_amount
            return
        # If there's not enough material in a stockpile, call self
        while stockpile[reaction] < required_amount:
            calculate_required(reaction)
            # Add the produced material to stockpile
            stockpile[reaction] += reactions[reaction].produces
        # Consume the material
        stockpile[reaction] -= required_amount


filename = "input"
with open(filename) as file:
    data = file.read().splitlines()

# Input parsing and data type creation
for line in data:
    ingredients, output = line.split(" => ")
    ingredientlist = ingredients.split(", ")
    # Dictionary of required ingredients for a reaction. {Name of reaction: amount produced}
    ingredientdict = {}
    for item in ingredientlist:
        amount, name = item.split(" ")
        ingredientdict[name] = int(amount)
    # Handling the reaction
    amount, name = output.split(" ")
    # Initializing the stockpile while the name variable is accessible
    stockpile[name] = 0
    # Reactions are in a dictionary for easy access. {Name of reaction: Reaction-object}
    reaction = Reaction(int(amount), ingredientdict)
    reactions[name] = reaction

calculate_required("FUEL")
print("\nDay 14 part 1 solution: " + str(ore_required))

