# Advent of Code 2019 - Day 2 - part Day 1

filename = "input"
with open(filename) as file:
    data = file.readline()
    inputs = data.split(",")
    inputs = [int(i) for i in inputs]

    inputs[1] = 12
    inputs[2] = 2

    index = 0
    while True:
        if inputs[index] == 1:
            inputs[inputs[index + 3]] = inputs[inputs[index + 1]] + inputs[inputs[index + 2]]
            index += 4
        elif inputs[index] == 2:
            inputs[inputs[index + 3]] = inputs[inputs[index + 1]] * inputs[inputs[index + 2]]
            index += 4
        elif inputs[index] == 99:
            print("Done")
            break
        else:
            print("Incorrect Opcode!")
            break
print(inputs[0])
