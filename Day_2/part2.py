# Advent of Code 2019 - Day 2 - part 2

class Memory:
    def __init__(self, values):
        self.values = values

    def read(self, address):
        return self.values[address]

    def write(self, address, value):
        self.values[address] = value

    def contents(self):
        return self.values

class Computer:
    def __init__(self, memory):
        self.ip = 0
        self.memory = memory

    def run(self):
        running = True
        while running:
            running = self.execute(self.memory.read(self.ip))

    def execute(self, instruction):
        if instruction == 1:
            self.add()
            self.ip += 4
        elif instruction == 2:
            self.multiply()
            self.ip += 4
        elif instruction == 99:
            #print("Done")
            return False
        else:
            #print("Error")
            return False
        return True

    def add(self):
        addr1 = self.memory.read(self.ip + 1)
        addr2 = self.memory.read(self.ip + 2)
        addr_result = self.memory.read(self.ip + 3)
        self.memory.write(addr_result, self.memory.read(addr1) + self.memory.read(addr2))

    def multiply(self):
        addr1 = self.memory.read(self.ip + 1)
        addr2 = self.memory.read(self.ip + 2)
        addr_result = self.memory.read(self.ip + 3)
        self.memory.write(addr_result, self.memory.read(addr1) * self.memory.read(addr2))


filename = "input"
with open(filename) as file:
    data = file.readline()
    data = data.split(",")
    data = [int(i) for i in data]
    orig_inputs = tuple(data)
    target = 19690720
    memory = Memory(orig_inputs)
    computer = Computer(memory)
    for noun in range(0, 100):
        for verb in range(0, 100):
            memory.__init__(list(orig_inputs))
            memory.write(1, noun)
            memory.write(2, verb)
            computer.__init__(memory)
            computer.run()
            if memory.read(0) == target:
                print(100 * noun + verb)
                break
        else:
            continue
        break
