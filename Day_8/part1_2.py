# Advent of Code 2019 - Day 8 - part 1 & 2

def countnumber(numlist, number):
    count = 0
    for digit in numlist:
        if int(digit) == number:
            count += 1
    return count

filename = "input"
with open(filename) as file:
    data = file.read()
x = 25
y = 6
layersize = x * y
layers = []

for slicepoint in range(layersize, len(data), layersize):
    layers += [data[slicepoint - layersize:slicepoint]]
highest = countnumber(layers[0], 0)
target_layer = ""

for layer in layers:
    result = countnumber(layer, 0)

    if result < highest:
        highest = result
        target_layer = layer

onecount = countnumber(target_layer, 1)
twocount = countnumber(target_layer, 2)
print("Part 1 result: " + str(onecount * twocount))

decoding_image = list(layers[len(layers) - 1])
for layer in reversed(layers):
    for pos, index in enumerate(layer):
        if int(index) == 0:
            decoding_image[pos] = " "
        elif int(index) == 1:
            decoding_image[pos] = "#"
print("")
print("Part 2 solution:")
final_image = "".join(decoding_image)
for row in range(x, layersize + 1, x):
    print(final_image[row - x:row])
