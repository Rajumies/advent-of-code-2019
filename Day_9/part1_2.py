# Advent of Code 2019 - Day 9 - part 1 & 2
# Re-wrote an improved intcode computer for this problem

import sys

class Memory:
    def __init__(self, values):
        self.values = values

    def read(self, address):
        self.check_address(address)
        return self.values[address]

    def write(self, address, value):
        self.check_address(address)
        self.values[address] = value

    # Checks if given address points to outside of current memory range
    def check_address(self, address):
        if address >= len(self.values):
            while address >= len(self.values):
                self.values += [0]
        if address < 0:
            sys.exit("Error: Negative memory address: " + str(address))

class Computer:
    def __init__(self, memory):
        self.ip = 0
        self.memory = memory
        self.input = []
        self.output = []
        self.running = False
        self.relative_base = 0

    def run(self):
        self.running = True
        while self.running:
            self.execute(self.memory.read(self.ip))

    def execute(self, instruction):
        ins = str(instruction)
        # Decode instruction to get opcode and parameter modes
        opcode, modes = self.decode_instruction(ins)
        # Get parameter addresses based on parameter modes
        param_addresses = self.read_params(modes)

        # Addition
        if opcode == "01":
            to_add1 = self.memory.read(param_addresses[0])
            to_add2 = self.memory.read(param_addresses[1])
            self.memory.write(param_addresses[2], to_add1 + to_add2)
            self.ip += 4

        # Multiplication
        elif opcode == "02":
            to_mul1 = self.memory.read(param_addresses[0])
            to_mul2 = self.memory.read(param_addresses[1])
            self.memory.write(param_addresses[2], to_mul1 * to_mul2)
            self.ip += 4

        # Input
        elif opcode == "03":
            invalue = self.input.pop(0)
            self.memory.write(param_addresses[0], invalue)
            self.ip += 2

        # Output
        elif opcode == "04":
            self.output += [self.memory.read(param_addresses[0])]
            self.ip += 2

        # Jump if true
        elif opcode == "05":
            value = self.memory.read(param_addresses[0])
            if value:
                self.ip = self.memory.read(param_addresses[1])
            else:
                self.ip += 3

        # Jump if not true
        elif opcode == "06":
            value = self.memory.read(param_addresses[0])
            if not value:
                self.ip = self.memory.read(param_addresses[1])
            else:
                self.ip += 3

        # If first less than second, store 1 in third position, else store 0
        elif opcode == "07":
            value1 = self.memory.read(param_addresses[0])
            value2 = self.memory.read(param_addresses[1])
            if value1 < value2:
                self.memory.write(param_addresses[2], 1)
            else:
                self.memory.write(param_addresses[2], 0)
            self.ip += 4

        # If first is equal to second, store 1 in third position, else store 0
        elif opcode == "08":
            value1 = self.memory.read(param_addresses[0])
            value2 = self.memory.read(param_addresses[1])
            if value1 == value2:
                self.memory.write(param_addresses[2], 1)
            else:
                self.memory.write(param_addresses[2], 0)
            self.ip += 4

        # Relative base offset
        elif opcode == "09":
            offset = self.memory.read(param_addresses[0])
            self.relative_base += offset
            self.ip += 2

        # Exit program
        elif opcode == "99":
            self.running = False
        # Invalid opcode
        else:
            sys.exit("Error: Unkown opcode: " + opcode)

    def decode_instruction(self, ins):
        while not len(ins) == 5:
            ins = "0" + ins
        opcode = ins[-2:]
        modes = ins[:-2]
        return opcode, modes

    def read_params(self, modes):
        """
        Reads three parameters following an instruction pointer
        :param modes: string containing parameter modes
        :return: List of parameter addresses based on parameter modes
        """
        params = []
        for index, mode in enumerate(reversed(modes), start=1):
            # check if we're trying to read instructions beyond current memory
            if self.ip + index >= len(self.memory.values):
                break
            # Position mode
            if int(mode) == 0:
                params += [self.memory.read(self.ip + index)]
            # Immediate mode
            elif int(mode) == 1:
                 params += [self.ip + index]
            # Relative mode
            elif int(mode) == 2:
                offset = self.memory.read(self.ip + index)
                params += [offset + self.relative_base]
            # Incorrect parameter mode
            else:
                sys.exit("Error: Unknown parameter mode: " + mode)

        return params

def test():
    testinput = ["109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99",
                "1102,34915192,34915192,7,4,7,99,0",
                "104,1125899906842624,99"]
    outputs = []
    for testrun in testinput:
        testdata = testrun.split(",")
        testdata = [int(i) for i in testdata]
        memory = Memory(testdata)
        computer = Computer(memory)
        computer.run()
        outputs += [computer.output]

    assert outputs[0] == [int(i) for i in testinput[0].split(",")], "Test 1 failed!"
    assert outputs[1] == [1219070632396864], "Test 2 failed!"
    assert outputs[2] == [1125899906842624], "Test 3 failed!"


filename = "input"
with open(filename) as file:
    data = file.readline()
data = data.split(",")
data = [int(i) for i in data]

test()

memory = Memory(data.copy())
computer = Computer(memory)

computer.input = [1]
computer.run()
print("\nDay 9 part 1 solution: " + str(computer.output[0]) + "\n")

memory.__init__(data.copy())
computer.__init__(memory)
computer.input = [2]
computer.run()
print("Day 9 part 2 solution: " + str(computer.output[0]))

