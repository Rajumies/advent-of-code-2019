# Advent of Code 2019 - Day 13 - part 1

from Day_13 import intcode

filename = "input"
with open(filename) as file:
    data = file.readline()
data = data.split(",")
data = [int(i) for i in data]

memory = intcode.Memory(data)
computer = intcode.Computer(memory)

computer.run()
blocks = 0
for i in range(2, len(computer.output), 3):
    if computer.output[i] == 2:
        blocks += 1
    if computer.output[i] == 4:
        print(computer.output[i - 2], computer.output[i - 1])
print("\nDay 13 part 1 solution: " + str(blocks))
