# Advent of Code - Day 4 - part 2

start = 265275
end = 781584

passwords = 0
for value in range(start, end + 1):
    oldvalue = 0
    doublenums = 0
    doubles = False
    increasing = True
    for number in str(value):
        if int(number) < oldvalue:
            increasing = False
            break
        if int(number) == doublenums:
            doubles = False
        elif not doubles and int(number) == oldvalue:
            doubles = True
            doublenums = int(number)
        oldvalue = int(number)
    if doubles and increasing:
        passwords += 1
print(passwords)
